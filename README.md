# OpenQAI

A modular billing and accounting web application.



## Origin


The principle is to simplify the setting up of estimates and invoices, but also to keep an accounting follow-up. 

The project originated to help craftsmen and save them time on these mandatory administrative tasks.

The goal is to make the project evolve via modules so that it adapts to the needs of each trade.

The first version will be provided with data rather oriented for electricians but nothing prevents other trades to use it.


## Architecture


The project is built in microservice architecture for the modular side (scalability objective of the project). 

This allows to have a microservice for a customer database, another one for the management of articles, another one for documents such as invoices and estimates and a last one for the accounting.

The advantage of a microservice architecture is that it allows for a moderate link between the different elements (interchangeability).



## Some Features


### Article Microservice


* Pre-filling of the lines with possibility of modification with recording of the modifications (ex: vat, article price, nomination, ...)



### Billing microservice


* Automatic indication of invoice and quote numbers

* Feature of import of invoice under certain format

* Export of quotes and invoices in PDF

* Feature to send quotes and invoices by email directly from the WebApp

* Parameter to change the template of the email sending estimate and invoice



### Accounting microservice


* Notification of unpaid invoice reminder

* Accounting management function



### User management microservice


* Possibility of account management via configurations (for non-local deployment)



## Built With


* [Maven](https://maven.apache.org/) - Dependency Management and Multi-module Management

* [Spring](https://spring.io/projects/spring-framework) - Used to manage the backend service like DAO and Manager

* [Bootstrap](https://getbootstrap.com/) -  a free and open-source CSS framework directed at responsive, mobile-first front-end web development





## Versioning


I use [Git](https://git-scm.com/) for versioning.



## Authors


* **Maxime Blaise** - [D4RT4GNaN](https://github.com/D4RT4GNaN)

